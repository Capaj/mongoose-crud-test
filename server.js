var config = require('config');
var mongoose = require('mongoose');
mongoose.connect(config.mongo);

var express = require('express');
var prepareApi = require('./lib/api');
var swig = require('swig');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', __dirname + '/views');

app.listen(3000);

module.exports = prepareApi.then(function(){
	app.use('/api', require('baucis')());
	require('./routes/user-controllers');

	console.log("rest api ready");
	return app;
});

