var serverReady = require('../server');
var userModel = require('../lib/users');

serverReady.then(function(app){
	app.get('/', function (req, res) {
		userModel.find({}).exec().then(function(docs){
			res.render('user/list', { users: docs});
		});
	});

	app.get('/edit/:id', function (req, res) {
		userModel.findById(req.params.id).exec().then(function(doc){
			console.log("doc", doc);
			res.render('user/edit', doc);
		});
	});

	app.get('/create', function (req, res) {
		res.render('user/create', {});
	});

});