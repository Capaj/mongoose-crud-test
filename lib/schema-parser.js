function isValidInt(n) {
	return n % 1 === 0 && n>=0 && n <= Number.MAX_SAFE_INTEGER;
}

module.exports = function(row) {
	var schema = {};
	Object.keys(row).forEach(function (key){
	    var val = row[key];
		var type = String;
		var asInt = parseInt(val);
		if (isValidInt(asInt)) {
			type = Number;
			if (key.match(/date|Date/)) {
				type = Date;
			}
		}
		schema[key] = type;
	});
	return schema;
};