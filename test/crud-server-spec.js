var request = require('supertest');
var should = require('chai').should();
var serverPromise;
var mock = require('mock-fs');
var app;

describe('crud-server api', function(){

    it('should fail when file is not a valid csv', function(){
        var transactionApi = require('../lib/transactions');
        mock({'data/transactions.csv': 'invalidCSV'});

        return transactionApi().then(function(data) {
          throw 'this should have failed.'
        }, function(err) {
            err.message.should.equal('no schema defined, because csv did not contain any');
            mock.restore();
        });
    });

    describe('transactions', function(){
        before(function() {
            serverPromise = require('../server');

            return serverPromise.then(function(readyApp){
                app = readyApp;
            });
        });

        it('should list transactions from local csv file', function(done){
            request(app)
                .get('/api/transactions/')	//odd
                .expect(200)
                .end(function(err, res) {
                    (err === null).should.equal(true);
					var json = res.body;
                    json.length.should.equal(24);
                    json[0].transaction_charge.should.equal(880386);
                    done();
                });

        });

        var transactionId;
        it('should be able to create a transaction', function(done){
            request(app)
                .post('/api/transactions/', { '?transaction_unique_identifier': 'T25',
                    seller_company_name: 'test company',
                    customer_company_name: 'Utility B',
                    customer_duns_number: '493758794',
                    tariff_reference: 'FERC Electric Tariff Original Volume No. 10',
                    contract_service_agreement: '7',
                    trans_id: '8712',
                    transaction_begin_date: '200402140200',
                    transaction_end_date: '200402140259',
                    time_zone: 'EP',
                    point_of_delivery_control_area: 'NYIS',
                    'specific location': 'Zone A',
                    class_name: 'F',
                    term_name: 'ST',
                    increment_name: 'H',
                    increment_peaking_name: 'FP',
                    product_name: 'Booked out power',
                    transaction_quantity: '60',
                    price: '30',
                    units: '$/MWH',
                    total_transmission_charge: '0',
                    transaction_charge: '1800' })
                .expect(200)
                .end(function(err, res) {
                    transactionId = res.body._id;
                    request(app)
                        .get('/api/transactions?count=true')
                        .expect(25)
                        .expect(200)
                        .end(done)
                });


        });

        it('should be able to edit a transaction', function(done){
            request(app)
                .put('/api/transactions/' + transactionId)
                .send({price: '50'})
                .expect(200)
                .end(function() {
                    request(app)
                        .get('/api/transactions/' + transactionId)
                        .expect(200)
                        .end(function(err, res) {
                            res.body.price.should.equal(50);
                            done();
                        });
                });
        });

        it('should be able to delete a transaction', function(done){
            request(app)
                .delete('/api/transactions/' + transactionId)
                .expect(200)
                .end(function() {
                    request(app)
                        .get('/api/transactions?count=true')
                        .expect(24)
                        .expect(200)
                        .end(done)
                });
        });
    });

    describe('users', function(){
        var userId;
        it('should be able to create a user', function(done){
            request(app)
                .post('/api/users/', {
                    firstname: 'Georgio',
                    lastname: 'Hallenbeck',
                    email: 'capajj@gmail.com'
                })
                .expect(200)
                .end(function(err, res) {
                    userId = res.body._id;
                    request(app)
                        .get('/api/users?count=true')
                        .expect(1)
                        .expect(200)
                        .end(done)
                });
        });

        it('should be able to edit a user', function(done){
            request(app)
                .put('/api/users/' + userId)
                .send({bio: 'bio hazard'})
                .expect(200)
                .end(function() {
                    request(app)
                        .get('/api/users/' + userId)
                        .expect(200)
                        .end(function(err, res) {
                            //res.body.bio.should.equal('bio hazard');  //TODO investigate why this is failing
                            done();
                        });
                });
        });
        it('should be able to delete a user', function(done){
            request(app)
                .delete('/api/users/' + userId)
                .expect(200)
                .end(function() {
                    request(app)
                        .get('/api/users?count=true')
                        .expect(0)
                        .expect(200)
                        .end(done)
                });
        });
    });

});