var schemaParser = require('../lib/schema-parser');
var should = require('chai').should();

describe('schema-parser', function(){
    it('should be able to detect a date type from key name', function(){
        var schema = schemaParser({transaction_begin_date: '200401010000'});
		schema.transaction_begin_date.should.equal(Date);
    });

	it('should recognize number type if a string is an Integer, everything else is a string type', function(){
	    var schema = schemaParser({
			tariff_reference_date: 'FERC Electric Tariff Original Volume No. 10',
			contract_service_agreement: '2'
		});

		schema.contract_service_agreement.should.equal(Number);
		schema.tariff_reference_date.should.equal(String);

	});
});