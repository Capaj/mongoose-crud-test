var mongoose = require('mongoose');
var baucis = require('baucis');

var validateEmail = function(email) {
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	return re.test(email)
};

var user = new mongoose.Schema({
	firstname: String,
	lastname: String,
	email: {
		type: String,
		trim: true,
		unique: true,
		required: 'Email address is required',
		validate: [validateEmail, 'Please fill a valid email address'],
		match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
	},
	password: {
		type: String,
		required: 'Password is required'
	},
	bio: String
});

module.exports = mongoose.model('user', user);
baucis.rest('user');

