Original requirements: https://drive.google.com/file/d/0B3mG7t57mkn1ZjdJY09Lc2czNlNoMnpwcDY1bE5NSTdXSVdr/view?usp=sharing

install the project dependencies and start with 
```
npm i
npm start
```

to run the tests
```
npm test
```

Tested under iojs 1.8.1, requires ES6 Promise, will not work under older node.js environments.

I would personally rather write a single page application than having the views rendered serverside, as that would be less demanding on server resources in heavy loads. In this case, I tried to adhere to the initial requirements as much as possible.

Post feedback notes: They did not like the test solution, so If you are interviewing with them, take this as an example of how NOT to do it. 