var csv = require('csv-parser')
var baucis = require('baucis');
var mongoose = require('mongoose');
var fs = require('fs');
var schemaParser = require('./schema-parser');
var debug = require('debug')('api:transactions');
var schema;
var transactions;
var transactionModel;

module.exports = function() {
	return new Promise(function (resolve, reject) {
		fs.createReadStream('./data/transactions.csv')
			.pipe(csv())
			.on('data', function(data) {
				if (!schema) {
					schema = schemaParser(data);
					transactions = new mongoose.Schema(schema);

					transactionModel = mongoose.model('transaction', transactions);
					baucis.rest('transaction');
				}
				transactionModel.create(data);
				console.log('row', data)
			}).on('end', function() {
				debug('end');
				if (!schema) {
					reject(new Error('no schema defined, because csv did not contain any'));
				}
				resolve(transactions);
			}).on('error', function(err) {
				debug("err", err);
				reject(err);
			});
	});
};

